/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_tool.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/17 18:39:08 by fldoucet          #+#    #+#             */
/*   Updated: 2018/12/03 11:31:18 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static int		cut_parse(t_list **load, char *str, char *temp, int i)
{
	t_list	*tmp;

	if (str[i] == '\n' && (str[i + 1] == '\n' || str[i + 1] == '\0'))
	{
		if (i > 30 && str[i - 21] != '\n')
			return (-1);
		if (!(temp = ft_strsub(str, i - 19, 19)))
			return (-1);
		if (!(tmp = ft_lstnew(temp, 20)))
			return (-1);
		ft_strdel(&temp);
		ft_firstadd(load, tmp);
	}
	return (1);
}

static int		ft_parse(char *str, t_list **load)
{
	int		i;
	char	*temp;

	if (!(temp = ft_strsub(str, 0, 19)))
		return (-1);
	if (!(*load = ft_lstnew(temp, 20)))
		return (-1);
	ft_strdel(&temp);
	i = 20;
	if (str[i] != '\0' && str[i] != '\n')
		return (-1);
	while (str[i])
	{
		if (cut_parse(load, str, temp, i) == -1)
			return (-1);
		i++;
	}
	ft_strdel(&str);
	return (1);
}

int				ft_swallow(int fd, t_list **load)
{
	int		rd;
	char	*ret;
	char	buf[BUFF_SIZE + 1];

	rd = 1;
	ret = NULL;
	while (rd != 0)
	{
		if ((rd = read(fd, buf, BUFF_SIZE)) == -1)
			return (-1);
		buf[rd] = '\0';
		if (!buf[3])
			return (-1);
		if (!(ret = ft_strjoin_free(ret, buf, 1)))
			return (-1);
	}
	return (ft_parse(ret, load));
}
