/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cut_tool.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/27 14:24:55 by edjubert          #+#    #+#             */
/*   Updated: 2018/11/29 18:08:19 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static int	blow_me(char **s)
{
	int	i;
	int	j;
	int	k;
	int	refs[2];

	refs[0] = 0;
	refs[1] = 4;
	i = 0;
	while (s[i])
	{
		k = 0;
		j = -1;
		while (s[i][++j])
		{
			if (s[i][j] == '#' && j > refs[0])
				refs[0] = j;
			if (s[i][j] == '#')
				k++;
		}
		refs[1] = k > 0 ? refs[1] + 1 - k : refs[1];
		i++;
	}
	refs[0] = refs[0] + 1 > refs[1] ? refs[0] + 1 : refs[1];
	return (refs[0]);
}

static char	**cut_tool(char **s)
{
	int		i;
	int		refs;
	char	**tmp;

	if (!(tmp = (char**)malloc(sizeof(char*) * 5)))
		return (NULL);
	refs = blow_me(s);
	i = -1;
	while (s[++i])
		if (ft_strchr(s[i], '#') != NULL)
			tmp[i] = ft_strsub(s[i], 0, refs);
		else if (refs - i > 0)
			tmp[i] = ft_strsub(s[i], 0, refs);
	ft_tabdel(&s);
	return (tmp);
}

void		cut_everything(t_list **load)
{
	t_list	*tmp;

	tmp = *load;
	while (tmp)
	{
		tmp->content = cut_tool(tmp->content);
		tmp = tmp->next;
	}
}
