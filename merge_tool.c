/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   merge_tool.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 19:02:22 by fldoucet          #+#    #+#             */
/*   Updated: 2018/11/27 12:09:41 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char	**ft_init(char **tetra)
{
	int i;

	i = ft_strlen(tetra[0]) - 1;
	while (i > 0)
	{
		tetra = ft_move_up(tetra, 1);
		tetra = ft_move_left(tetra);
		i--;
	}
	return (tetra);
}

char	**ft_unmerge(char **map, char **tetra)
{
	int i;
	int j;

	i = 0;
	while (tetra[i])
	{
		j = 0;
		while (tetra[i][j])
		{
			if (tetra[i][j] != '.')
				map[i][j] = '.';
			j++;
		}
		i++;
	}
	return (map);
}

char	**ft_merge(char **map, char **tetra)
{
	int i;
	int j;

	i = 0;
	while (map[i])
	{
		j = 0;
		while (map[i][j])
		{
			if (map[i][j] == '.' && tetra[i][j] != '.')
				map[i][j] = tetra[i][j];
			j++;
		}
		i++;
	}
	return (map);
}
