/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/18 15:19:22 by fldoucet          #+#    #+#             */
/*   Updated: 2018/11/30 17:33:55 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# include "libft/libft.h"
# include <fcntl.h>

# define BUFF_SIZE	32

int		ft_swallow(int fd, t_list **load);
int		ft_check(t_list **load);
void	ft_fillit(t_list **load);
int		ft_usage(void);
int		ft_print_error(void);
void	ft_print_map(char **map);
void	ft_print_lst(t_list *load);
void	ft_trans(t_list **load);
int		ft_fill(char **map, t_list *tmp);
void	ft_resize_chain(t_list **load, int new_size);
char	**ft_move_right(char **tab);
char	**ft_move_left(char **tab);
char	**ft_move_up(char **tab, int sec);
char	**ft_move_down(char **tab);
char	**ft_merge(char **map, char **tetra);
char	**ft_catching(char **tab);
char	**ft_unmerge(char **map, char **tetra);
char	**ft_init(char **tetra);
int		ft_check_end(char **tetra);
char	**ft_mall(int size);
char	**ft_tetradup(char **tetra);
int		ft_check_pos(char **map, char **tetra);
void	cut_everything(t_list **load);
void	ft_lst_del(t_list **load);

#endif
