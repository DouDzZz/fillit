/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_tool.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/19 14:21:08 by edjubert          #+#    #+#             */
/*   Updated: 2018/11/28 18:31:36 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		ft_usage(void)
{
	char *usage;

	usage = "usage: fillit file";
	ft_putendl_fd(usage, 1);
	return (-1);
}

int		ft_print_error(void)
{
	ft_putendl_fd("error", 1);
	return (-1);
}

void	ft_print_map(char **map)
{
	int i;

	i = 0;
	while (map[i])
		ft_putendl(map[i++]);
}

void	ft_print_lst(t_list *load)
{
	t_list *tmp;

	tmp = load;
	while (tmp)
	{
		ft_putendl("haha :");
		ft_putendl(tmp->content);
		tmp = tmp->next;
	}
}
