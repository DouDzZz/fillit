NAME=fillit
CSOURCE=./main.c \
		./check_tool.c \
		./read_tool.c \
		./print_tool.c \
		./fillit.c \
		./trans_tool.c \
		./backtracking_tool.c \
		./move_tool.c \
		./merge_tool.c \
		./cut_tool.c
HSOURCE=./fillit.h
OSOURCE=$(CSOURCE:.c=.o)
LIBFTFOLDER=./libft/
LIBFT=libft.a
FLAGS=-Wall -Wextra -Werror
GCC=gcc

all: $(NAME)

%.o: %.c $(HSOURCE)
	$(GCC) $(FLAGS) -c $(CSOURCE) -I $(HSOURCE)

$(NAME): $(OSOURCE)
	make -C $(LIBFTFOLDER) re
	$(GCC) $(FLAGS) -o $(NAME) $(OSOURCE) $(LIBFTFOLDER)$(LIBFT)

clean:
	/bin/rm -rf $(OSOURCE)
	make -C $(LIBFTFOLDER) clean

fclean: clean
	/bin/rm -rf $(NAME)
	make -C $(LIBFTFOLDER) fclean

re: fclean all

buildlib:
	make -C $(LIBFTFOLDER) re
	make -C $(LIBFTFOLDER) clean

devbuild: buildlib re clean
	/bin/rm -rf $(OSOURCE)

.PHONY: all clean fclean re buildlib devbuild
