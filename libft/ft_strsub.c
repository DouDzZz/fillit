/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/17 16:41:54 by fldoucet          #+#    #+#             */
/*   Updated: 2018/11/27 15:52:14 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char	*rahc;
	size_t	i;

	i = 0;
	if (!(rahc = (char*)malloc(sizeof(char) * len + 1)))
		return (NULL);
	if (s)
	{
		while (i < len)
		{
			rahc[i] = s[start];
			i++;
			start++;
		}
		rahc[i] = '\0';
		return (rahc);
	}
	return (NULL);
}
