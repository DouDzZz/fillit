/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_countword.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/17 16:41:51 by fldoucet          #+#    #+#             */
/*   Updated: 2018/11/29 17:02:10 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_countword(char *str, int mode, int word, char sep)
{
	int i;
	int count;

	i = 0;
	count = -1;
	while (str[i])
	{
		if (str[i] == sep)
			i++;
		else
		{
			count++;
			if (mode == 1 && count == word)
				return (i);
			while (str[i] != sep && str[i])
				i++;
		}
	}
	return (count + 1);
}
