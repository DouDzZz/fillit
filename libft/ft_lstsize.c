/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstsize.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/17 16:41:52 by fldoucet          #+#    #+#             */
/*   Updated: 2018/11/21 11:34:31 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_lstsize(t_list *alst)
{
	t_list	*tmp;
	int		i;

	i = 0;
	tmp = alst;
	while (tmp)
	{
		tmp = tmp->next;
		i++;
	}
	return (i);
}
