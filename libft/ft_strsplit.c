/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/17 16:41:54 by fldoucet          #+#    #+#             */
/*   Updated: 2018/11/29 16:58:53 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	**ft_strsplit(char const *s, char c)
{
	int		i;
	int		words;
	char	**ret;

	if (s)
	{
		i = 0;
		words = ft_countword((char*)s, 0, 0, c);
		if (!(ret = (char**)malloc(sizeof(char*) * (words + 1))))
			return (NULL);
		while (i < words)
		{
			ret[i] = ft_getword((char*)s, i, c);
			i++;
		}
		ret[i] = 0;
		return (ret);
	}
	return (NULL);
}
