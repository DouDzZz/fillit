/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getword.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/17 16:41:51 by fldoucet          #+#    #+#             */
/*   Updated: 2018/11/29 17:01:55 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_getword(char *str, int words, char sep)
{
	int		i;
	int		j;
	int		k;
	char	*word;

	j = ft_countword(str, 1, words, sep);
	i = j;
	k = 0;
	while (str[i] && str[i] != sep)
		i++;
	if (!(word = malloc(sizeof(char) * (i - j))))
		return (0);
	while (j < i)
	{
		word[k] = str[j];
		k++;
		j++;
	}
	word[k] = '\0';
	return (word);
}
