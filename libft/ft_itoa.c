/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 15:26:17 by edjubert          #+#    #+#             */
/*   Updated: 2018/11/14 11:04:18 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	intlen(int n)
{
	int i;

	i = 1;
	while (n /= 10)
		i++;
	return (i);
}

char		*ft_itoa(int n)
{
	int		i;
	long	nb;
	int		neg;
	char	*res;

	if (!(res = (char*)malloc(sizeof(char) * (intlen(n) + 1))))
		return (NULL);
	neg = n < 0;
	i = 0;
	ft_bzero(res, (intlen(n) + 1));
	nb = neg ? (long)n * -1 : (long)n;
	if (nb == 0)
		res[i] = '0';
	while (nb)
	{
		res[i] = nb % 10 + 48;
		nb = nb / 10;
		i++;
	}
	if (neg)
		res[i] = '-';
	ft_strrev(res);
	res[intlen(n) + 1] = '\0';
	return (res);
}
