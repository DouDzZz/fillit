/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_tool.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 19:02:22 by fldoucet          #+#    #+#             */
/*   Updated: 2018/11/29 16:56:40 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char	**ft_catching(char **tab)
{
	int i;
	int j;
	int k;

	j = ft_strlen(tab[0]) - 1;
	i = -1;
	k = 0;
	if (ft_check_end(tab) == 0)
		return (tab);
	while (++i < j)
		if (tab[i][j] != '.')
			k = 1;
	if (k == 1)
	{
		ft_move_down(tab);
		while (j > 0)
		{
			ft_move_left(tab);
			j--;
		}
	}
	else
		ft_move_right(tab);
	return (tab);
}

char	**ft_move_up(char **tab, int sec)
{
	int		i;
	int		j;
	char	tmp;

	j = -1;
	if (sec == 1)
	{
		while (tab[0][++j])
			if (tab[0][j] != '.')
				return (tab);
	}
	i = 0;
	while (tab[i])
	{
		j = 0;
		while (tab[i] && tab[i + 1] && tab[i][j] && tab[i + 1][j])
		{
			tmp = tab[i][j];
			tab[i][j] = tab[i + 1][j];
			tab[i + 1][j] = tmp;
			j++;
		}
		i++;
	}
	return (tab);
}

char	**ft_move_down(char **tab)
{
	int i;
	int k;

	k = ft_strlen(tab[0]) - 1;
	i = 0;
	while (tab[k][++i])
		if (tab[k][i] != '.')
			return (tab);
	k = ft_strlen(tab[0]) - 1;
	while (k > 0)
	{
		tab = ft_move_up(tab, 0);
		k--;
	}
	return (tab);
}

char	**ft_move_left(char **tab)
{
	int		i;
	int		j;
	char	tmp;

	i = -1;
	while (tab[++i])
		if (tab[i][0] != '.')
			return (tab);
	i = 0;
	while (tab[i])
	{
		j = 0;
		while (tab[i][j] && tab[i][j + 1])
		{
			tmp = tab[i][j];
			tab[i][j] = tab[i][j + 1];
			tab[i][j + 1] = tmp;
			j++;
		}
		i++;
	}
	return (tab);
}

char	**ft_move_right(char **tab)
{
	int		i;
	int		j;
	char	tmp;

	i = -1;
	j = ft_strlen(tab[0]) - 1;
	while (tab[++i])
		if (tab[i][j] != '.')
			return (tab);
	i = 0;
	while (tab[i])
	{
		j = ft_strlen(tab[i]) - 1;
		while (j > 0)
		{
			tmp = tab[i][j];
			tab[i][j] = tab[i][j - 1];
			tab[i][j - 1] = tmp;
			j--;
		}
		i++;
	}
	return (tab);
}
