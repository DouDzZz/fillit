/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/18 18:22:20 by fldoucet          #+#    #+#             */
/*   Updated: 2018/11/30 17:31:37 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void			ft_lst_del(t_list **load)
{
	while ((*load))
	{
		ft_tabdel((char***)&(*load)->content);
		free(*load);
		*load = (*load)->next;
	}
}

char			**ft_mall(int size)
{
	int		i;
	int		k;
	char	**map;

	i = 0;
	if (!(map = malloc(sizeof(char *) * (size + 1))))
		return (0);
	while (i < size)
	{
		if (!(map[i] = malloc(sizeof(char) * (size + 1))))
			return (0);
		k = 0;
		while (k < size)
		{
			map[i][k] = '.';
			k++;
		}
		map[i][k] = '\0';
		i++;
	}
	map[i] = NULL;
	return (map);
}

void			ft_fillit(t_list **load)
{
	char	**map;
	int		map_min;
	int		ret;
	t_list	*tmp;

	tmp = *load;
	ret = 0;
	map_min = ft_sqrt((ft_lstsize(tmp) * 4));
	map = ft_mall(map_min);
	ft_resize_chain(load, map_min);
	ret = ft_fill(map, tmp);
	while (map && ret != 1)
	{
		map_min++;
		ft_tabdel(&map);
		map = ft_mall(map_min);
		ft_resize_chain(load, map_min);
		ret = ft_fill(map, tmp);
	}
	ft_print_map(map);
	ft_tabdel(&map);
	ft_lst_del(load);
}
