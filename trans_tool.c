/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   trans_tool.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 16:01:34 by fldoucet          #+#    #+#             */
/*   Updated: 2018/12/03 11:34:06 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static char		**ft_resize(char **tetra, int new_size, int k)
{
	char	**ret;
	int		i;
	int		j;

	if (!(ret = ft_mall(new_size)))
		return (NULL);
	i = 0;
	if ((int)ft_strlen(tetra[0]) > new_size)
		return (tetra);
	while (tetra[i] && i < new_size)
	{
		j = 0;
		while (tetra[i][j])
		{
			if (tetra[i][j] != '.')
				ret[i][j] = k;
			j++;
		}
		i++;
	}
	ft_tabdel(&tetra);
	return (ret);
}

void			ft_resize_chain(t_list **load, int new_size)
{
	t_list	*tmp;
	int		k;

	k = 'A';
	tmp = *load;
	while (tmp)
	{
		tmp->content = ft_resize(tmp->content, new_size, k);
		tmp->content = ft_init(tmp->content);
		tmp = tmp->next;
		k++;
	}
}

void			ft_trans(t_list **load)
{
	t_list	*tmp;
	char	*tmp2;
	int		i;

	tmp = *load;
	while (tmp)
	{
		i = 0;
		tmp2 = tmp->content;
		tmp->content = ft_strsplit(tmp->content, '\n');
		ft_strdel(&tmp2);
		tmp->content = ft_init(tmp->content);
		tmp = tmp->next;
	}
	cut_everything(load);
}
