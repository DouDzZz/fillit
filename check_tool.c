/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_tool.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/18 15:27:01 by fldoucet          #+#    #+#             */
/*   Updated: 2018/11/29 17:57:20 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static int		ft_check_connection(char *str)
{
	int i;
	int co;

	i = 0;
	co = 0;
	while (i < 19)
	{
		if (str[i] == '#')
		{
			if (str[i + 1] && str[i + 1] == '#')
				co++;
			if (str[i - 1] && str[i - 1] == '#')
				co++;
			if (str[i - 5] && str[i - 5] == '#')
				co++;
			if (str[i + 5] && str[i + 5] == '#')
				co++;
		}
		i++;
	}
	if (co < 6)
		return (0);
	return (1);
}

static int		ft_count(char *str)
{
	int i;
	int hash;
	int point;
	int eol;

	i = 0;
	hash = 0;
	point = 0;
	eol = 0;
	while (i < 19)
	{
		if (str[i] == '#')
			hash++;
		if (str[i] == '.')
			point++;
		if (str[i] == '\n')
			eol++;
		i++;
	}
	if (hash == 4 && point == 12 && eol == 3)
		return (1);
	return (0);
}

static int		ft_check_tetra(char *str)
{
	int i;

	i = 0;
	while (i < 19)
	{
		if (str[i] == '#')
		{
			if ((str[i + 1] && str[i + 1] == '#')
					|| (str[i - 1] && str[i - 1] == '#')
					|| (str[i - 5] && str[i - 5] == '#')
					|| (str[i + 5] && str[i + 5] == '#'))
				i++;
			else
				return (0);
		}
		i++;
	}
	return (1);
}

static int		ft_check_char(char *str)
{
	int i;

	i = 0;
	while (i < 19 && (str[i] == '.' || str[i] == '#' || str[i] == '\n'))
		i++;
	if (str[i] == '\0')
		return (1);
	return (0);
}

int				ft_check(t_list **load)
{
	t_list *tmp;

	tmp = *load;
	while (tmp)
	{
		if (ft_check_char(tmp->content) && ft_count(tmp->content)
				&& ft_check_tetra(tmp->content)
				&& ft_check_connection(tmp->content))
			tmp = tmp->next;
		else
			return (1);
	}
	if (ft_lstsize(*load) > 26)
		return (1);
	return (0);
}
