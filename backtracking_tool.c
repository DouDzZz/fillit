/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   backtracking_tool.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/22 13:35:03 by fldoucet          #+#    #+#             */
/*   Updated: 2018/11/28 18:54:29 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		ft_check_pos(char **map, char **tetra)
{
	int i;
	int j;

	i = 0;
	while (tetra[i])
	{
		j = 0;
		while (tetra[i][j])
		{
			if (!map[i][j])
				return (0);
			if (map[i][j] != '.' && tetra[i][j] != '.')
				return (0);
			j++;
		}
		i++;
	}
	return (1);
}

int		ft_check_end(char **tetra)
{
	int i;
	int j;
	int ret;

	j = ft_strlen(tetra[0]) - 1;
	i = -1;
	ret = 0;
	while (++i <= j)
		if (tetra[i][j] != '.')
			ret = 1;
	if (ret == 1)
	{
		i = -1;
		while (++i <= j)
			if (tetra[j][i] != '.')
				return (0);
	}
	return (1);
}

int		ft_fill(char **map, t_list *tmp)
{
	char **tetra;

	if (!tmp)
		return (1);
	tetra = tmp->content;
	while (ft_check_end(tetra))
	{
		if (ft_check_pos(map, tetra) == 1)
		{
			map = ft_merge(map, tetra);
			if (ft_fill(map, tmp->next) == 1)
				return (1);
			map = ft_unmerge(map, tetra);
		}
		tetra = ft_catching(tetra);
	}
	if (ft_check_pos(map, tetra) == 1)
	{
		map = ft_merge(map, tetra);
		if (ft_fill(map, tmp->next) == 1)
			return (1);
		map = ft_unmerge(map, tetra);
	}
	tetra = ft_init(tetra);
	return (0);
}
