/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/17 18:47:07 by fldoucet          #+#    #+#             */
/*   Updated: 2018/11/30 17:33:06 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		main(int ac, char **av)
{
	t_list	*load;
	int		fd;

	if (ac != 2)
		return (ft_usage());
	if ((fd = open(av[1], O_RDONLY)) == -1)
		return (ft_print_error());
	if (ft_swallow(fd, &load) == -1)
		return (ft_print_error());
	if (ft_check(&load) == 1)
		return (ft_print_error());
	ft_trans(&load);
	ft_fillit(&load);
	return (0);
}
